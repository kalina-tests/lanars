<?php
/**
 * Plugin Name: Most Comment Post
 * Author: SKalina
 */


add_shortcode('showPost', function () {
    return Mcp::init();
});

add_image_size('mcpThumb', 200, 100, false);

wp_enqueue_style( 'style', plugins_url( 'css/style.css' , __FILE__ ) );

class Mcp
{

    public static function init()
    {

        $post = self::getPost();

        return ($post->comment_count > 0 ? self::render($post) : '');
    }

    private static function getPost()
    {
        $args = [
            'post_type' => 'post',
            'posts_per_page' => 1,
            'orderby' => 'wp_posts.comment_count',
            'order' => 'ASC'
        ];

        $result = new WP_Query($args);

        return array_shift($result->posts);
    }

        /**
     * the title, short description, picture, publication date and the actual
     * number of comments must be displayed.
     * @param null $post
     * @return string
     */
    public static function render($post = null)
    {
        $thumb = get_the_post_thumbnail($post->ID, 'mcpThumb');
        $excerpt = get_the_excerpt($post);
        $date = date_format(date_create($post->post_date),"d.m.Y H:i:s");

        return "
        <h1>{$post->post_title}</h1>
        <p>{$excerpt}</p>
        <div>
            <div class='mcp-img'>{$thumb}</div>
        </div>
        <p>{$date}</p>
        <p>{$post->post_content}</p>
        <p>{$post->comment_count}</p>
        ";
    }
    
}


